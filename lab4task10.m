syms x1(t) x2(t);

equations1 = [diff(x1, t) == 2*x1-1, diff(x2,t) == 3*x1+2*x2+1];
conditions1 = [x1(0) == 1, x2(0) == 1];

equations2 = [diff(x1, t) == x1+x2, diff(x2,t) == 2*x1+2];
conditions2 = [x1(0) == 1, x2(0) == 1];

solutions1 = dsolve(equations1, conditions1);
solutions2 = dsolve(equations2, conditions2);

disp("Equations 1:");
disp("  " + string(equations1(1)));
disp("  " + string(equations1(2)));
disp("Conditions:");
disp(join(string(conditions1)));
disp("Solutions:");
disp("  x1(t) = " + string(solutions1.x1));
disp("  x2(t) = " + string(solutions1.x2));

disp("Equations 2:");
disp("  " + string(equations2(1)));
disp("  " + string(equations2(2)));
disp("Conditions:");
disp(join(string(conditions2)));
disp("Solutions:");
disp("  x1(t) = " + string(solutions2.x1));
disp("  x2(t) = " + string(solutions2.x2));

figure(1);
fplot(solutions1.x1);
hold on;
fplot(solutions1.x2);
grid on;
hold off;

figure(2);
fplot(solutions2.x1);
hold on;
fplot(solutions2.x2);
grid on;
hold off;