syms x z;

a = x^2+3*x+1;
b = (2*x^2+1)/sqrt(x+2);
c = asin(2*x);
d = sqrt(x-3)+ tan(2*x)/4;
e = x - cos(x);
f = sin(x^2)+x^2;
g = (0.5^x)*cos(x);
h = 5*(sin(x)-cos(5*x));
k = sin(z+3)-2^x;
l =  sin(x) - cos(2*z);


disp("y(x) = " + string(a) +  newline + "   dy/dx = " + string(diff(a, x, 1)));
disp("y(x) = " + string(b) +  newline + "   dy/dx = " + string(diff(b, x, 1))); 
disp("y(x) = " + string(c) +  newline + "   dy/dx = " + string(diff(c, x, 1))); 
disp("y(x) = " + string(d) +  newline + "   dy/dx = " + string(diff(d, x, 1))); 
disp("y(x) = " + string(e) +  newline + "   dy/dx = " + string(diff(e, x, 1))); 
disp("y(x) = " + string(f) +  newline + "   dy/dx = " + string(diff(f, x, 1))); 
disp("y(x) = " + string(g) +  newline + "   dy/dx = " + string(diff(g, x, 1))); 
disp("y(x, z) = " + string(h) +  newline + "   dy/dx = " + string(diff(h, x, 1))); 
disp("y(x, z) = " + string(k) +  newline + "   dy/dx = " + string(diff(k, x, 1))); 
disp("y(x, z) = " + string(l) +  newline + "   dy/dx = " + string(diff(l, x, 1)));