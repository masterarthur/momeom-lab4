syms x y;

a = 2*x+(y^2)/2;
b = 18*(x^2)*(y^2)+32*(x^3)*y;
c = 4*x^3+3*y^2;
d = 18*(x^2)*(y^2)+32*(x^3)*y;

cInt = int(int(c, x, -2, 1), y, -1, 2);

disp('∫∫' + string(a) + "dxdy=" + string(int(int(a,x), y)) + "+C");
disp('∫∫' + string(b) + "dxdy=" + string(int(int(b,x), y)) + "+C");
disp('∫ (2, -1) ∫ (1, -2)' + string(c) + "dxdy=" + string(cInt) + "=" + string(vpa(cInt)));
disp('∫∫' + string(d) + "dxdy=" + string(int(int(d,x), y)) + "+C");