syms x;

a = x^2-cos(x);
b = exp(-2*x) + x^3;
c = exp(x) + (x^4)/3;

disp('y(x) = ' + string(a))
disp("      y''= " + string(diff(a, x, 2)));
disp("     y'''= " + string(diff(a, x, 3)));
disp("  y''''''= " + string(diff(a, x, 6)));

disp('y(x) = ' + string(b))
disp("      y''= " + string(diff(b, x, 2)));
disp("     y'''= " + string(diff(b, x, 3)));
disp("  y''''''= " + string(diff(b, x, 6)));

disp('y(x) = ' + string(c))
disp("      y''= " + string(diff(c, x, 2)));
disp("     y'''= " + string(diff(c, x, 3)));
disp("  y''''''= " + string(diff(c, x, 6)));