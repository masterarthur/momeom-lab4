syms x;

a = sin(x);
b = (x^2-cos(2*x));
c = sin(x) + (x^2)/2;
d = (0.5^x)*cos(x);
e = sin(2*x+3) - 2*cos(5*x);
f = exp(x)*sin(x+3);

aInt = int(a,x, 1, 3);
bInt = int(b,x, 1, 3);
cInt = int(c,x, 1, 3);
dInt = int(d,x, 1, 3);
eInt = int(e,x, 1, 3);
fInt = int(f,x, 1, 3);

disp('∫ (3, 1) ' + string(a) + "dx=" + string(aInt) + "=" + string(vpa(aInt)));
disp('∫ (3, 1) ' + string(b) + "dx=" + string(bInt) + "=" + string(vpa(bInt)));
disp('∫ (2, 1) ' + string(c) + "dx=" + string(cInt) + "=" + string(vpa(cInt)));
disp('∫ (5, 0) ' + string(d) + "dx=" + string(dInt) + "=" + string(vpa(dInt)));
disp('∫ (4, 0) ' + string(e) + "dx=" + string(eInt) + "=" + string(vpa(eInt)));
disp('∫ (0, -5) ' + string(f) + "dx=" + string(fInt) + "=" + string(vpa(fInt)));