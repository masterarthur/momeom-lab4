syms x;

a = cos(x);
b = x^2;
c = (exp(x)-x);
d = 3*x+log(x);
e = x^3 + 3*x^2+1;
f = 1/x;
g = sqrt(x^2+1);
h = sqrt(x+1)/(x^2+2*x);

disp('∫' + string(a) + "dx=" + string(int(a,x)) + "+C");
disp('∫' + string(b) + "dx=" + string(int(b,x)) + "+C");
disp('∫' + string(c) + "dx=" + string(int(c,x)) + "+C");
disp('∫' + string(d) + "dx=" + string(int(d,x)) + "+C");
disp('∫' + string(e) + "dx=" + string(int(e,x)) + "+C");
disp('∫' + string(f) + "dx=" + string(int(f,x)) + "+C");
disp('∫' + string(g) + "dx=" + string(int(g,x)) + "+C");
disp('∫' + string(h) + "dx=" + string(int(h,x)) + "+C");

