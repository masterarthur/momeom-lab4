syms x y z;

a = (x^2-2)*y+3*z;
b = sin(x) - 2*y+exp(z);

aInt = int(int(int(a, x, 0, 3), y, 1, 2), z, -1, 1);
bInt = int(int(int(b, x, -1, 1), y, 2, 3), z, 1, 3);


disp('∫ (1, -1) ∫ (2, 1) ∫ (3, 0) ' + string(a) + "dxdydz=" + string(aInt) + "=" + string(vpa(aInt)));
disp('∫ (3, 1) ∫ (3, 2) ∫ (1, -1) ' + string(b) + "dxdydz=" + string(bInt) + "=" + string(vpa(bInt)));