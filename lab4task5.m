syms x y;

a = [((x-3)/(x^2-4*x+3)), 3];
b = [((sqrt(7+x)-sqrt(7-x))/(5*x)), 0];
c = [(1/sin(x)-1/tan(x)), 0];
d = [((x+2)/(x^2-x-6)), inf];
e = [(sin(x)^tan(x)), pi/2];
f = [((2-sqrt(x))/(sqrt(5*x+1)-5)), 4];
g = [((sqrt(x^2-3)-1)/(sqrt(x^2-5)-2)), 3];
h = [(sin(5*x)/sin(6*x)), pi];
k = [((3*y-sqrt(x+9))/(x+y)), 3];
l = [((3*x+2*y)/(5*x-3*y)), 1];

disp("lim x-> " + string(a(2)) + " " + string(a(1)) + " = " + string(vpa(limit(a(1), x, a(2)))));
disp("lim x-> " + string(b(2)) + " " + string(b(1)) + " = " + string(vpa(limit(b(1), x, b(2))))); 
disp("lim x-> " + string(c(2)) + " " + string(c(1)) + " = " + string(vpa(limit(c(1), x, c(2))))); 
disp("lim x-> " + string(d(2)) + " " + string(d(1)) + " = " + string(vpa(limit(d(1), x, d(2))))); 
disp("lim x-> " + string(e(2)) + " " + string(e(1)) + " = " + string(vpa(limit(e(1), x, e(2))))); 
disp("lim x-> " + string(f(2)) + " " + string(f(1)) + " = " + string(vpa(limit(f(1), x, f(2), "right")))); 
disp("lim x-> " + string(g(2)) + " " + string(g(1)) + " = " + string(vpa(limit(g(1), x, g(2), "left")))); 
disp("lim x-> " + string(h(2)) + " " + string(h(1)) + " = " + string(vpa(limit(h(1), x, h(2))))); 
disp("lim x-> " + string(k(2)) + " " + string(k(1)) + " = " + string(limit(k(1), x, k(2)))); 
disp("lim x-> " + string(l(2)) + " " + string(l(1)) + " = " + string(limit(l(1), x, l(2))));