syms x(t);

eq1 = diff(x, t) == t*cos(t) + x/t;
condition1  = x(1) == 0;

eq2 = diff(x, t) == 1/(exp(t-x));
condition2  = x(1) == 1;

eq3 = diff(x, t) == (1+x^2)/(1+t^2);
condition3  = x(0) == 1;

eq4 = diff(x, t) + x == cos(t);
condition4  = x(0) == 1/2;

eq5 = diff(x, t) - x*tan(t) == 1/cos(t);
condition5  = x(0) == 1/2;

eq6 = (x+t)*diff(x, t) == 1;
condition6  = x(-1) == 0;

eq7 = (1+exp(t))*x*diff(x, t) == exp(t);
condition7  = x(0) == 1;

eq8 = diff(x, t) == (t^2)*x^4-x/t;
condition8  = x(1) == 1/(3^(1/3));

solution1 = dsolve(eq1, condition1); 
solution2 = dsolve(eq2, condition2); 
solution3 = dsolve(eq3, condition3); 
solution4 = dsolve(eq4, condition4); 
solution5 = dsolve(eq5, condition5); 
solution6 = dsolve(eq6, condition6); 
solution7 = dsolve(eq7, condition7); 
solution8 = dsolve(eq8, condition8);

disp(string(eq1) + " " + string(condition1) + " =>");
disp(solution1); 
disp(string(eq2) + " " + string(condition2) + " =>");
disp(solution2); 
disp(string(eq3) + " " + string(condition3) + " =>");
disp(solution3); 
disp(string(eq4) + " " + string(condition4) + " =>");
disp(solution4); 
disp(string(eq5) + " " + string(condition5) + " =>");
disp(solution5); 
disp(string(eq6) + " " + string(condition6) + " =>");
disp(solution6); 
disp(string(eq7) + " " + string(condition7) + " =>");
disp(solution7); 
disp(string(eq8) + " " + string(condition8) + " =>");
disp(solution8);

figure(1);
fplot(solution1, [-10,10]); 
grid on;
figure(2);
fplot(solution2, [-10,10]); 
grid on;
figure(3);
fplot(solution3, [-10,10]); 
grid on;
figure(4);
fplot(solution4, [-10,10]); 
grid on;
figure(5);
fplot(solution5, [-10,10]); 
grid on;
figure(6);
fplot(solution6, [-10,10]); 
grid on;
figure(7);
fplot(solution7, [-10,10]); 
grid on;
figure(8);
fplot(solution8, [-10,10]);
grid on;
