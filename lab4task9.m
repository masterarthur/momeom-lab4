syms x(t);

Dx = diff(x,t);

eq1 = diff(x, t, 2) + 4*x == 0;
conditions1  = [x(0) == 0, Dx(0) == 2];

eq2 = diff(x, t, 2) + 3*diff(x, t) + 2*x == 0;
conditions2  = [x(0) == 1, Dx(0) == -1];

eq3 = diff(x, t, 2) + 2*diff(x, t) == 1;
conditions3  = [x(0) == 1, Dx(0) == 0];

eq4 = t*diff(x, t, 2) == diff(x, t);
conditions4  = [x(0) == 0, Dx(0) == 0];


solution1 = dsolve(eq1, conditions1); 
solution2 = dsolve(eq2, conditions2); 
solution3 = dsolve(eq3, conditions3); 
solution4 = dsolve(eq4, conditions4); 


disp(string(eq1) + " " + join(string(conditions1)) + " =>");
disp(solution1); 
disp(string(eq2) + " " + join(string(conditions2)) + " =>");
disp(solution2); 
disp(string(eq3) + " " + join(string(conditions3)) + " =>");
disp(solution3); 
disp(string(eq4) + " " + join(string(conditions4)) + " =>");
disp(solution4); 

